import { Routes } from '@angular/router';
import { MapamigosComponent } from './mapamigos/mapamigos.component';
import { LoginComponent } from './login/login.component';

export const ROUTER: Routes = [
  { path: '', component: LoginComponent },
  { path: 'mapa', component: MapamigosComponent }
];
