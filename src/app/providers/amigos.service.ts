import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AmigosModel } from '../Models/amigos.model';
import { LoginService } from './login.service';

const URL = 'http://localhost/'; // Colocar aqui o endereço do Backend

// @Injectable({
//   providedIn: 'root'
// })
@Injectable()
export class AmigosService {

  constructor(private http: HttpClient , private loginService: LoginService) { }

  getAll() {
    let headers = new HttpHeaders();
    if (this.loginService.Islogged()) {
      headers = headers.set('Authorization', `Bearer ${this.loginService.user.token}`);
    }
    return this.http.get<AmigosModel>(`${URL}/api/amigos`, {headers: headers});
  }

  getClosed(id: number) {
    return this.http.get<AmigosModel>(`${URL}/api/amigos/${id}`);
  }

}
