import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { User } from '../Models/user.model';

const URL = 'http://localhost/'; // Colocar aqui o endereço do Backend

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
// @Injectable()
export class LoginService {

  constructor(private http: HttpClient) { }

  user: User;
  Islogged(): boolean {
    if (this.user.token !== '') {
      return true;
    }
  }

  login(email: string, password: string): Observable<User> {
    return this.http.post<User>(`${URL}/api/amigos`,
                {'username': email, 'password': password }, httpOptions)
                .pipe(tap(user => this.user = user));
  }

}
