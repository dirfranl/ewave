import { TestBed, inject } from '@angular/core/testing';

import { AmigosService } from './amigos.service';

describe('AmigosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AmigosService]
    });
  });

  it('should be created', inject([AmigosService], (service: AmigosService) => {
    expect(service).toBeTruthy();
  }));
});
