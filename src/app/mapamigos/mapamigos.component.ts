import { Component, OnInit, ViewChild } from '@angular/core';
import { AmigosModel } from '../Models/amigos.model';
import { AmigosService } from '../providers/amigos.service';
import * as _ from 'lodash';
import { } from '@types/googlemaps';
import { LoginService } from '../providers/login.service';



@Component({
  selector: 'app-mapamigos',
  templateUrl: './mapamigos.component.html',
  styleUrls: ['./mapamigos.component.scss']
})
export class MapamigosComponent implements OnInit {

  amigos: AmigosModel;
  amigosPertos: AmigosModel;
  locations: any;
  Selected: number;

  title = 'app';
  tiles = [
    { color: 'green' }
  ];

  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map;

  iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';

  icons = {
    parking: {
      name: 'Parking',
      icon: this.iconBase + 'parking_lot_maps.png'
    },
    library: {
      name: 'Library',
      icon: this.iconBase + 'library_maps.png'
    },
    info: {
      name: 'Info',
      icon: this.iconBase + 'info-i_maps.png'
    }
  };


  bounds = new google.maps.LatLngBounds();

  constructor(private _listaAmigos: AmigosService) { }



  ngOnInit() {

    this._listaAmigos.getAll().subscribe(
      data => { this.amigos = data; },
      err => console.error(err),
      // () => console.log(this.amigos[0].nome + ' ' + Object.keys(this.amigos).length),
    );

  }

  criaOption(event: Event, id: number) {
    // event.preventDefault();
    this.amigosPertos = undefined;
    this._listaAmigos.getClosed(id).subscribe(data => {this.amigosPertos = data;
      const mapOptions = {
        zoom: 16,
        center: new google.maps.LatLng(this.amigosPertos[0].lat, this.amigosPertos[0].lng),
        mapTypeId: 'roadmap'
      };
      this.initMap(mapOptions);
    });

  }

  initMap(mapOptions: any) {

    this.locations = _.values(this.amigosPertos);
    this.locations.push(this.amigos[this.Selected]);
    this.locations[3].type = ' ';
    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapOptions);
    let marker;

    this.locations.map(location => {
      const center = new google.maps.LatLng(location.lat, location.lng);
      let icone;
      if (location.type !== ' ') {
        icone = this.icons[location.type].icon;
         marker = new google.maps.Marker({ animation: google.maps.Animation.DROP, position: center , icon: icone, map: this.map });

      } else {
        marker = new google.maps.Marker({ animation: google.maps.Animation.DROP, position: center , map: this.map });
      }
      this.bounds.extend(marker.position);
    });
    this.map.fitBounds(this.bounds);
  }


}
