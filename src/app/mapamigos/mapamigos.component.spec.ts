import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapamigosComponent } from './mapamigos.component';

describe('MapamigosComponent', () => {
  let component: MapamigosComponent;
  let fixture: ComponentFixture<MapamigosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapamigosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapamigosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
