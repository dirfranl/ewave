import { Component } from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { LoginService } from '../providers/login.service';
import {Router} from '@angular/router';
import { ROUTER } from '../app.router';


/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

emailFormControl = new FormControl('', [
  Validators.required,
  Validators.email,
]);

senhaFormControl = new FormControl('', [
  Validators.required
]);

matcher = new MyErrorStateMatcher();

constructor(private router: Router, private loginService: LoginService) {}

cancela() {
  this.emailFormControl.reset();
  this.senhaFormControl.reset();
}

login() {
  this.loginService.login(this.emailFormControl.value, this.senhaFormControl.value)
      .subscribe(user => {// console.log(user);
        this.router.navigate(['/mapa']); } , err => console.log(err));

}

}
