import { Component, ViewChild, HostListener, ElementRef, OnInit } from '@angular/core';
// import { AmigosService } from '../app/providers/amigos.service';
// import { throwError } from 'rxjs';
// import { } from '@types/googlemaps';
// import { AmigosModel } from './Models/amigos.model';
// import * as _ from 'lodash';
// import { Button } from 'protractor';
// import { Input } from '@angular/compiler/src/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  // setMapType(mapTypeId: string) {
  //   this.map.setMapTypeId(mapTypeId)
  // }

  // setCenter() {
  //   this.map.setCenter(new google.maps.LatLng(this.latitude, this.longitude));

  //   const location = new google.maps.LatLng(this.latitude, this.longitude);

  //   const marker = new google.maps.Marker({
  //     position: location,
  //     map: this.map,
  //     title: 'Got you!'
  //   });

  //  marker.addListener('click', this.simpleMarkerHandler);

  //   marker.addListener('click', () => {
  //     this.markerHandler(marker);
  //   });
  // }

  simpleMarkerHandler() {
    alert('Simple Component\'s function...');
  }

  markerHandler(marker: google.maps.Marker) {
    alert('Marker\'s Title: ' + marker.getTitle());
  }

}
