import { NgModule } from '@angular/core';
import { MatToolbarModule,
         MatButtonModule,
         MatInputModule,
         MatIconModule,
         MatSelectModule,
         MatCardModule,
         MatDividerModule} from '@angular/material';
import {RouterModule} from '@angular/router';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import { MapamigosComponent } from './mapamigos/mapamigos.component';
import { ROUTER } from './app.router';
import { LoginComponent } from './login/login.component';

import { LoginService } from './providers/login.service';
import { AmigosService } from './providers/amigos.service';


@NgModule({
  declarations: [
    AppComponent,
    MapamigosComponent,
    LoginComponent
  ],
  imports: [
    FormsModule,
    MatDividerModule,
    MatSelectModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    MatToolbarModule,
    MatCardModule,
    ReactiveFormsModule,
    RouterModule.forRoot(ROUTER)
  ],
  providers: [
    AmigosService,
    LoginService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
